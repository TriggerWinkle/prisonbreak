﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Access : Pickup
{
    public int AccessID;

    protected override Item CreateItem()
    {
        return new AccessItem(ObjectName, Weight, AccessID);
    }
}

