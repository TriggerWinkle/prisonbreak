﻿using UnityEngine;

[System.Serializable]
public class AccessItem : Item
{ 
    public int DoorID;

    public AccessItem(string Name, float Weight, int DoorID) : base(Name, Weight)
    {
        this.Name = Name;
        this.Weight = Weight;
        this.DoorID = DoorID;
    }
}
