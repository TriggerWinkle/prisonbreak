﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Boat : MonoBehaviour, IInteractable
{
    public void Action()
    {
        Sail();
    }

    public void Sail()
    {
        SceneManager.LoadScene(0);
    }
}
