﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bonus : Pickup
{
    public int Points;

    protected override Item CreateItem()
    {
        return new BonusItem(ObjectName, Weight, Points);
    }
}
