﻿using UnityEngine;

[System.Serializable]
public class BonusItem : Item
{
    public int Points;

    public BonusItem(string Name, float Weight, int Points) : base(Name, Weight)
    {
        this.Name = Name;
        this.Weight = Weight;
        this.Points = Points;
    }
}
