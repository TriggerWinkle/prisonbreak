﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    Camera main;

    [Header("POV variables")]
    [Space(10)]
    public float desiredPov = 60;
    public float currentPov;

    public GameObject playerObject;

    void Start()
    {
        main = GetComponentInChildren<Camera>();
        ResetPlayerObject();
    }

    public void ResetPlayerObject()
    {

    }

    void FixedUpdate()
    {
        currentPov = main.fieldOfView;

        transform.position = Vector3.Lerp(transform.position, playerObject.transform.position, 0.08f);

        transform.Rotate(0, -Input.GetAxis("Joy X"), 0);

        if (Input.GetAxis("Joy Y") > 0.2f)
        {
            main.fieldOfView = main.fieldOfView + Input.GetAxis("Joy Y");
        }

        if (Input.GetKey(KeyCode.UpArrow) && desiredPov > 30)
        {
            desiredPov--;
        }
        if (Input.GetKey(KeyCode.DownArrow) && desiredPov < 90)
        {
            desiredPov++;
        }
        if (Input.GetKey(KeyCode.W))
        {
            if (playerObject.GetComponent<PlayerController>() != null && playerObject.GetComponent<PlayerController>().IsFrozen())
            {
                main.fieldOfView = Mathf.Lerp(main.fieldOfView, desiredPov + 20, 0.05f);
            }
        }
        else if (Input.GetKey(KeyCode.S))
        {
            if (playerObject.GetComponent<PlayerController>() != null && playerObject.GetComponent<PlayerController>().IsFrozen())
            {
                main.fieldOfView = Mathf.Lerp(main.fieldOfView, 70, 0.05f);
            }
        }
        else
        {
            main.fieldOfView = Mathf.Lerp(main.fieldOfView, desiredPov, 0.05f);
        }
    }
}
