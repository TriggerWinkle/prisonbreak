﻿using UnityEngine;
using TMPro;

public class CenterText : MonoBehaviour
{
    public static CenterText Instance = null;

    private TextMeshProUGUI _centerText;
    private Animator _animator;

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this) Destroy(this.gameObject);

        _centerText = GetComponent<TextMeshProUGUI>();
        _animator = GetComponent<Animator>();
    }

    public void PrintCenterText(string a_Text)
    {
        _centerText.text = a_Text;
        _animator.SetTrigger("ShowText");
    }
}
