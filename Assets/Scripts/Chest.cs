﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chest : MonoBehaviour, IInteractable
{
    private bool _isOpened;

    private Animator _animator;

    public GameObject[] Treasure;

    private AudioSource _audioSource;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
    }

    public void Action()
    {
        if (_isOpened) return;

        _isOpened = true;

        _audioSource.Play();

        _animator.SetBool("IsOpened", _isOpened);

        StartCoroutine(SpawnItems());
    }

    private IEnumerator SpawnItems()
    {
        Torch[] t = GetComponentsInChildren<Torch>();
        for (int i = 0; i < t.Length; i++)
        {
            t[i].StopTorch();
        }
        for (int i = 0; i < Treasure.Length; i++)
        {
            Rigidbody r = Instantiate(Treasure[i], transform.position, transform.rotation).GetComponent<Rigidbody>();
            r.AddRelativeForce(new Vector3(8, 4, Random.Range(6, 5)));

            yield return new WaitForSeconds(1);
        }
    }
}
