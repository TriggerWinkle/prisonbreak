﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyParticleSystem : MonoBehaviour
{
    void Start()
    {
        Destroy(this.gameObject, 2);
    }
}
