﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableObject : MonoBehaviour, IAttackable
{
    public float Health;
    public GameObject Item;
    public GameObject DestroyedParticles;

    public void TakeDamage(int damage)
    {
        Health -= damage;

        if (Health < 0)
        {
            if (Item) Instantiate(Item, transform.position, Quaternion.Euler(0, 0, 0));
            if (DestroyedParticles) Instantiate(DestroyedParticles, transform.position, Quaternion.Euler(0, 0, 0));
            Destroy(this.gameObject);
        }
    }
}
