﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour, IInteractable
{
    private Animator _animator;
    private bool _isLocked = true;

    public int DoorId;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public void Action()
    {
        if (!_isLocked) return;

        if (!DoorId.Equals(-1) && !Inventory.Instance.HasKey(DoorId))
        {
            CenterText.Instance.PrintCenterText("You do not have the key for this door.");
            return;
        }

        _isLocked = false;
        _animator.SetBool("IsLocked", _isLocked);
        Inventory.Instance.TryAndRemoveItem(Inventory.Instance.GetKey(DoorId));
    }
}
