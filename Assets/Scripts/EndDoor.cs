﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EndDoor : MonoBehaviour, IInteractable
{
    private Lock[] _locks;

    private Animator _animator;

    private void Start()
    {
        _locks = GetComponentsInChildren<Lock>();
        _animator = GetComponent<Animator>();
    }

    public void Action()
    {
        int unlockedDoors = 0;
        for (int i = 0; i < _locks.Length; i++)
        {
            if(_locks[i].Unlock())
            {
                unlockedDoors++;
            }
        }
        if (unlockedDoors == _locks.Length)
        {
            _animator.SetBool("IsOpened", true);
            Invoke("ChangeScene", 2);
        }
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene(1);
    }
}
