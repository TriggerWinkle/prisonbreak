﻿using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class Enemy : MonoBehaviour, IAttackable
{
	protected Animator _animator;

    public float Speed;

    public float HealthPoints;

    private NavMeshAgent _navMeshAgent;

    private Coroutine _hitCooldownCoroutine;

    public AudioClip[] HurtSounds;

    private void Start () 
	{
		_animator = GetComponent<Animator>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
	}

    private void Update () 
	{
        Vector3 direction = PlayerController.Instance.transform.position - this.transform.position;
		direction.y = 0;
        float angleToPlayer = (Vector3.Angle(direction, transform.forward));

        if (angleToPlayer >= -80 && angleToPlayer <= 80 && Vector3.Distance(PlayerController.Instance.transform.position, this.transform.position) < 10)
		{
			if(direction.magnitude > 1.25f)
			{


                _navMeshAgent.isStopped = false;

                _navMeshAgent.SetDestination(PlayerController.Instance.transform.position);
				_animator.SetBool("IsWalking",true);
				_animator.SetBool("IsAttacking",false);
			}
			else
			{
                _animator.SetBool("IsAttacking",true);
				_animator.SetBool("IsWalking",false);
            }
		}
		else 
		{
            _navMeshAgent.isStopped = true;

            _navMeshAgent.ResetPath();

            _animator.SetBool("IsWalking", false);
            _animator.SetInteger("IdleState", Random.Range(0, 1));
			_animator.SetBool("IsAttacking", false);
        }
    }

    public void TakeDamage(int damage)
    {
        HealthPoints -= damage;
        if (HealthPoints < 0)
        {
            Destroy(this.gameObject);
        }

        _hitCooldownCoroutine = StartCoroutine(HitCooldown());

        SoundManager.Instance.PlayClip(HurtSounds);
    }

    private IEnumerator HitCooldown()
    {
        yield return new WaitForSeconds(0.5f);

        StopCoroutine(_hitCooldownCoroutine);
        _hitCooldownCoroutine = null;
    }
}
