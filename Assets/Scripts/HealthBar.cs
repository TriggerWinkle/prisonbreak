﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HealthBar : MonoBehaviour
{
    public static HealthBar Instance = null;

    public Image HealthImage;
    public TextMeshProUGUI HealthText;

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this) Destroy(this.gameObject);
    }

    public void SetHealth(float a_HealthPoints)
    {
        HealthImage.fillAmount = a_HealthPoints / 100;
        HealthText.text = a_HealthPoints + "%";
    }
}
