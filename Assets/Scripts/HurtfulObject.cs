﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtfulObject : MonoBehaviour
{
    public float MinDmg, MaxDmg;

    public HurtType HurtType;

    public AudioClip[] HitSounds;

    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.GetComponent<IAttackable>() != null && collision.gameObject.layer == (int)HurtType)
        {
            collision.gameObject.GetComponent<IAttackable>().TakeDamage((int)Random.Range(MinDmg, MaxDmg));

            SoundManager.Instance.PlayClip(HitSounds);
        }
    }
}

public enum HurtType
{
    HurtPlayer = 10,
    HurtEnemy = 11
};