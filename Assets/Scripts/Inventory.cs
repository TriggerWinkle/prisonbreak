﻿using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Inventory : MonoBehaviour
{
    public static Inventory Instance = null;

    public float MaximumWeight;

    public float CurrentWeight;

    public List<Item> ItemInventory = new List<Item>();

    private int _score = 0;

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else if (Instance != this)
            Destroy(this);

        DontDestroyOnLoad(this.gameObject);
    }

    public float CalculateWeight()
    {
        float j = 0;

        for (int i = 0; i < ItemInventory.Count; i++)
        {
            j += ItemInventory[i].Weight;
        }

        CurrentWeight = j;

        return j;
    }

    public bool TryAndAddItem(Item item)
    {
        if (ItemInventory.Contains(item)) return false;
        if (CurrentWeight + item.Weight >= MaximumWeight) return false;

        ItemInventory.Add(item);

        CalculateWeight();

        CenterText.Instance.PrintCenterText("Added item " + item.Name);

        return true;
    }

    public void AddScore(int i)
    {
        _score += i;
        UIManager.Instance.ScoreText.text = _score.ToString();
    }

    public bool TryAndRemoveItem(Item item)
    {
        if (!ItemInventory.Contains(item)) return false;

        ItemInventory.Remove(item);

        CalculateWeight();

        Debug.Log("Removed item " + item.Name);

        return true;
    }

    public bool HasKey(int id)
    {
        for (int i = 0; i < ItemInventory.Count; i++)
        {
            if(ItemInventory[i] is AccessItem)
            {
                AccessItem key = (AccessItem)ItemInventory[i];
                if (key.DoorID == id)
                {
                    return true;
                }
                else return false;
            }
        }

        return false;
    }

    public AccessItem GetKey(int id)
    {
        for (int i = 0; i < ItemInventory.Count; i++)
        {
            if (ItemInventory[i] is AccessItem)
            {
                AccessItem key = (AccessItem)ItemInventory[i];
                if (key.DoorID == id)
                {
                    return (AccessItem)ItemInventory[i];
                }
            }
        }

        return null;
    }
}