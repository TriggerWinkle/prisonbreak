﻿using UnityEngine;

[System.Serializable]
public class Item
{
    public string Name;
    public float Weight;

    public Item(string Name, float Weight)
    {
        this.Name = Name;
        this.Weight = Weight;
    }
}
