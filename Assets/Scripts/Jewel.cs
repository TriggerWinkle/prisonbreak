﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jewel : MonoBehaviour
{
    public int Value;
    public AudioClip[] PickupSounds;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Inventory.Instance.AddScore(Value);

            SoundManager.Instance.PlayClip(PickupSounds);

            Destroy(this.gameObject);
        }
    }
}
