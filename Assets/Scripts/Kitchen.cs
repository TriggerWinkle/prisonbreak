﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kitchen : MonoBehaviour, IInteractable
{
    public Transform SpawnPosition;

    public GameObject[] ObjectsToSpawn;

    private bool _isUsed = false;

    public void Action()
    {
        if (_isUsed) return;

        StartCoroutine(SpawnItems());

        Debug.Log("Spawned");
    }

    private IEnumerator SpawnItems()
    {
        _isUsed = true;

        for (int i = 0; i < ObjectsToSpawn.Length; i++)
        {
            Rigidbody r = Instantiate(ObjectsToSpawn[i], SpawnPosition.position, transform.rotation).GetComponent<Rigidbody>();
            r.AddRelativeForce(new Vector3(8, 4, Random.Range(6, 5)));

            yield return new WaitForSeconds(1);
        }
    }
}
