﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lock : MonoBehaviour
{
    private Animator _animator;
    private bool _isLocked = true;

    public int DoorId;

    private void Start()
    {
        _animator = GetComponent<Animator>();
    }

    public bool Unlock()
    {
        if (!_isLocked) return true;

        if (!DoorId.Equals(-1) && !Inventory.Instance.HasKey(DoorId))
        {
            return false;
        }

        _isLocked = false;
        _animator.SetBool("IsLocked", _isLocked);
        Inventory.Instance.TryAndRemoveItem(Inventory.Instance.GetKey(DoorId));

        return true;
    }
}
