﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pickup : MonoBehaviour
{
    public string ObjectName;
    public float Weight;
    private GameObject InventoryObj;

    public bool IsInInventory()
    {
        return InventoryObj != null;
    }

    public void SetInventoryObj(GameObject go)
    {
        InventoryObj = go;
    }

    public void RemoveInventoryObj()
    {
        Destroy(InventoryObj);
        InventoryObj = null;
    }

    public void Respawn()
    {
        RemoveInventoryObj();
        transform.position = Camera.main.transform.position + Camera.main.transform.forward;
        gameObject.SetActive(true);
    }

    protected abstract Item CreateItem();

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            if (Inventory.Instance.TryAndAddItem(CreateItem()))
            {
                Destroy(this.gameObject);
            }
        }
    }
}