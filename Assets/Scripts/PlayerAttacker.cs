﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttacker : MonoBehaviour
{
    private PlayerController _playerController;

    public AudioClip[] AttackSounds;

    private void Awake()
    {
        _playerController = GetComponent<PlayerController>();
    }

    private void Update()
    {
        if (_playerController.IsFrozen()) return;

        if (Input.GetButtonDown("Fire1"))
        {
            _playerController.RigidBody.velocity = new Vector3(0, 0, 0);

            _playerController.Animator.SetInteger("AttackAnimation", Random.Range(0, 3));
            _playerController.Animator.SetTrigger("Attack");
            _playerController.Animator.SetBool("IsRunning", false);
            _playerController.Animator.SetBool("IsWalking", false);

            _playerController.TakeAction();

            SoundManager.Instance.PlayClip(AttackSounds);
        }
    }
}
