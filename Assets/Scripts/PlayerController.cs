﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using TMPro;

public class PlayerController : MonoBehaviour, IAttackable
{
    public static PlayerController Instance = null;

    [HideInInspector] public Rigidbody RigidBody;

    [HideInInspector] public Animator Animator;

    private float _walkSpeed = 2f;

    private bool _isFrozen = false;

    private int _healthPoints;
    public int MinHealth, MaxHealth;

    public AudioClip[] HurtSounds;

    public bool IsFrozen()
    {
        return _isFrozen;
    }

    private Coroutine _hitCooldownCoroutine;

    public void TakeAction()
    {
        StartCoroutine(Attack());
    }

    private IEnumerator Attack()
    {
        _isFrozen = true;
        yield return new WaitForSeconds(1.75f);
        _isFrozen = false;
    }

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this) Destroy(this.gameObject);

        RigidBody = GetComponent<Rigidbody>();

        Animator = GetComponentInChildren<Animator>();
    }

    private void Start()
    {
        Mathf.Clamp(_healthPoints, MinHealth, MaxHealth);

        _healthPoints = MaxHealth;

        HealthBar.Instance.SetHealth(_healthPoints);
    }

    private void Update()
    {
        if (!_isFrozen)
        {
            if (Input.GetAxis("Vertical") > 0 || Input.GetAxis("Horizontal") > 0 || Input.GetAxis("Vertical") < -0f || Input.GetAxis("Horizontal") < -0f)
            {
                Animator.SetBool("IsWalking", true);
                if (Input.GetButton("Run"))
                {
                    Animator.SetBool("IsRunning", true);
                    _walkSpeed = 4.5f;
                }
                else
                {
                    Animator.SetBool("IsRunning", false);
                    _walkSpeed = 2f;
                }
            }
            else
            {
                Animator.SetBool("IsWalking", false);
            }

            Vector3 forwardDir = new Vector3(Camera.main.transform.forward.x, 0, Camera.main.transform.forward.z);

            Vector3 targetDir = forwardDir * Input.GetAxis("Vertical") * _walkSpeed;

            Vector3 normalizedTargetDir = targetDir.normalized;

            Vector3 rigidDir = Vector3.Lerp(RigidBody.velocity, targetDir, 1);
            rigidDir = new Vector3(rigidDir.x, RigidBody.velocity.y, rigidDir.z);

            RigidBody.velocity = rigidDir;

            if (normalizedTargetDir != Vector3.zero)
            {
                float targetRotation = Mathf.Atan2(normalizedTargetDir.x, normalizedTargetDir.z) * Mathf.Rad2Deg;
                float smoothRotate = 0; //I needed to make a variable out of this because the method under this statement wants a ref to a variable.
                transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref smoothRotate, 0.1f);
            }

            transform.Rotate(new Vector3(0, Input.GetAxis("Horizontal") * 5, 0));
        }
    }

    public void TakeDamage(int damage)
    {
        if (_hitCooldownCoroutine != null) return;

        if(_healthPoints - damage <= 0)
        {
            Destroy(this.gameObject);
        }

        _healthPoints -= damage;

        SoundManager.Instance.PlayClip(HurtSounds);

        HealthBar.Instance.SetHealth((float)_healthPoints);

        _hitCooldownCoroutine = StartCoroutine(HitCooldown());
    }

    private IEnumerator HitCooldown()
    {
        yield return new WaitForSeconds(0.5f);

        StopCoroutine(_hitCooldownCoroutine);
        _hitCooldownCoroutine = null;
    }
}