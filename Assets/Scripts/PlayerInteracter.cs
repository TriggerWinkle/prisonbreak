﻿using UnityEngine;

public class PlayerInteracter : MonoBehaviour
{
    private PlayerController _playerController;

    public KeyCode InteractButton = KeyCode.E;

    private void Awake()
    {
        _playerController = GetComponent<PlayerController>();
    }

    public void Interact()
    {
        Ray r = new Ray(transform.position, transform.forward);

        RaycastHit hit;
        
        Debug.DrawRay(r.origin, r.direction, Color.red, 1f);

        int ignorePlayer = ~LayerMask.GetMask("Player");

        if (Physics.Raycast(r, out hit, 2f, ignorePlayer))
        {
            IInteractable i = hit.collider.gameObject.GetComponent<IInteractable>();
            if(i != null)
            {
                i.Action();
            }
        }
    }

    private void Update()
    {
        if (_playerController.IsFrozen()) return;

        if (Input.GetKeyDown(InteractButton))
        {
            _playerController.RigidBody.velocity = new Vector3(0, 0, 0);

            _playerController.Animator.SetBool("IsRunning", false);
            _playerController.Animator.SetBool("IsWalking", false);
            _playerController.Animator.SetTrigger("Action");

            _playerController.TakeAction();

            Interact();
        }
    }
}
