﻿using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using TreeEditor;
using UnityEngine;

public class ProceduralBehaviour : MonoBehaviour
{
    public static ProceduralBehaviour Instance;
    public float PerlinSeedX, PerlinSeedY;
    public int Seed = 0;
    public Terrain Terrain;
    public int WorldSize = 10;
    public float MaxHeight = 600;

    private ProceduralTerrain _proceduralTerrain;
    
    public List<HeightPass> Passes;

    private void Awake()
    {
        if (!Instance)
            Instance = this;
        else if 
            (Instance != this) Destroy(this);

        SetSeed(Seed);
    }

    private void Start()
    {
        _proceduralTerrain = new ProceduralTerrain(WorldSize + 13, WorldSize + 13, Passes);
        Generate();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Generate(); 
        } 
    }

    public void Generate()
    {
        SetSeed(Seed);
        Debug.Log("We can generate " + int.MaxValue + " versions of this procedure");
        Debug.Log("Generating version: " + Seed);

        _proceduralTerrain.Generate();

        float[,] norm = _proceduralTerrain.GetHeightsNormalized();
        
        Terrain.terrainData.SetHeights(0,0,norm);
        Texture2D mask = new Texture2D(WorldSize, WorldSize);
        Color[] colors = new Color[WorldSize*WorldSize];

        for (int x = 0; x < WorldSize; x++)
        {
            for (int z = 0; z < WorldSize; z++)
            {
                colors[x + (z*WorldSize)] = new Color(norm[x,z], norm[x,z], norm[x,z]);
            }
        }

        mask.SetPixels(colors);
        mask.Apply();
        
        //t.terrainData.terrainLayers[0].diffuseTexture = mask;
    }

    public void SetSeed(int s)
    {
        Random.InitState(s);
        if (Seed != s)
            Seed = s;
        PerlinSeedX = Random.Range(0.0f, 1000f);
        PerlinSeedY = Random.Range(0.0f, 1000f);

    }
}
