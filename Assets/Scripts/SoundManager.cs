﻿using UnityEngine;

public class SoundManager : MonoBehaviour
{
    private AudioSource _audioSource;

    public static SoundManager Instance = null;

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this) Destroy(this.gameObject);

        _audioSource = GetComponent<AudioSource>();
    }

    public void PlayClip(AudioClip[] a_AudioClips)
    {
        _audioSource.PlayOneShot(a_AudioClips[Random.Range(0, a_AudioClips.Length)]);
    }
}
