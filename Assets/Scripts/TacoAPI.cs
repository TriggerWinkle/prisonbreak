﻿using SimpleJSON;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using TMPro;

public class TacoAPI : MonoBehaviour
{
    public TextMeshPro RecipeText;

    private IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            if (webRequest.isNetworkError)
            {
                Debug.Log(": Error: " + webRequest.error);
            }
            else
            {
                Debug.Log(":\nReceived: " + webRequest.downloadHandler.text);

                Taco Ch = new Taco();
                JsonUtility.FromJsonOverwrite(webRequest.downloadHandler.text, Ch);

                RecipeText.text = Ch.seasoning.recipe;

            }
        }
    }

    private void Start()
    {
        StartCoroutine(GetRequest("http://taco-randomizer.herokuapp.com/random/"));
    }
}

[System.Serializable]
public class Taco
{
    public Seasoning seasoning = new Seasoning();
}

[System.Serializable]
public class Seasoning
{
    public string recipe;
}