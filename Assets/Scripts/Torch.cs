﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torch : MonoBehaviour
{
    private ParticleSystem[] _particleSystems;
    private GameObject _light;
    private AudioSource[] _audioSources;

    private void Start()
    {
        _particleSystems = GetComponentsInChildren<ParticleSystem>();
        _light = GetComponentInChildren<Light>().gameObject;
        _audioSources = GetComponentsInChildren<AudioSource>();
    }

    public void StopTorch()
    {
        for (int i = 0; i < _particleSystems.Length; i++)
        {
            _particleSystems[i].Stop();
        }
        for (int i = 0; i < _audioSources.Length; i++)
        {
            _audioSources[i].Stop();
        }
        _light.SetActive(false);
    }

    public void StartToch()
    {
        for (int i = 0; i < _particleSystems.Length; i++)
        {
            _particleSystems[i].Play();
        }
        for (int i = 0; i < _audioSources.Length; i++)
        {
            _audioSources[i].Play();
        }
        _light.SetActive(true);
    }
}
