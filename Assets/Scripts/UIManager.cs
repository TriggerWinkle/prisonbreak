﻿using UnityEngine;
using TMPro;

public class UIManager : MonoBehaviour
{
    public static UIManager Instance = null;

    public TextMeshProUGUI ScoreText;

    private void Awake()
    {
        if (!Instance) Instance = this;
        else if (Instance != this) Destroy(this.gameObject);
    }

    private void Start()
    {
        Inventory.Instance.AddScore(0);
    }
}
